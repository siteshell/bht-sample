#
# bhtarticleconverter.rb -- Sample Code
#
# Author: Peter D Bethke
#
# Problem - the BHT articles list is not in a responsive format
#
# Solution  - using the Nokogiri library (http://www.nokogiri.org/), the contents of the BHT main page located at
# http://bht.virginia.edu is turned into an object, which can then be manipulated and modified in-place. The articles
# list is located in a div of class newsticker-jcarousellite. So, I instruct Nokogiri to capture that block using
# a query language called xpath. This block is then parsed by iterating over each occurance of the <li>, or list item
# element. The various important bits, such as title, date, preview etc are captured to be merged into the target
# template. The target template is a responsive template I found by a quick search on the internet. It is a simple
# article list demo with some filter code built in. The articles in it are contained in a <tbody> container. To prep
# the template, I removed all the rows except one - which I left for reference. I then created a number of ruby
# functions to build the inner parts of the articles lists using Nokogiri, which allows a programmer to assemble html
# or xml using objects and render them to text (html or xml) at run-time. I then re-assembled the rows in the format
# expected by the "incoming" template but using the data from BHT, merging the data to create the new display list.
# The advanatage of this technique is that the local template can be edited outside the <tbody> area without disrupting
# the merging.
# ****Note: As long as the "path" to the <tbody> remains the same, the code will work. By path, I mean the nested path,
# as expressed by the xpath '//html//body//div[@id="article-list"]//section//div//div//div//div[@class="table-container"
# ]//table'. If this "path" is changed, the xpath will not locate the block. Its also possible to write the xpath using
# a "fuzzy" technique that ignores the "path" but I prefer an explicit path initially. If this script went into
# production there would be a lot more error trapping to account for changes like this. In addition, a test suite
# would be present to simulate scenarios in which this might occur.
#
# Delivery - The re-parsed html is not written to a file - rather is is passed as a variable to the mini web server
# Webrick, and is viewable at http://localhost:9090/ once the script is run.
#
# Future/Thoughts - because the articles from BHT are broken up and made into objects, they can be re-arranged in many
# ways, and re-formatted in many ways based on intial script parameters. The contents can also be turned into other
# formats like json (which would be easliy read by a javascript node appliciation, for example), or xml. Either of these
# formats could then be used to create a REST api (at least for reading). The articles could also be exported in .csv or
# .txt format, or written locally into html files. The key is that the script functions as a pass-through proxy requiring
# no altering of the existing BHT site. This might be a way to prototype or even introduce new layouts without having
# to alter the existing BHT codebase.


require 'open-uri'
require 'Nokogiri'
require 'webrick'
require 'active_support/all'

include WEBrick

def get_local_template_obj(path)
  File.open(path) { |f| Nokogiri::HTML(f) }
end

def get_article_elements(remote_obj, xpath)
  remote_obj.xpath(xpath)
end

def get_remote_obj(url)
  Nokogiri::HTML(open(url))
end


def build_checkbox_col(obj, index)
  # This is part of the "imcoming" template. Build it from scratch using nokogiri objs
  col = Nokogiri::XML::Node.new "td", obj
  ckbox_div = Nokogiri::XML::Node.new "div", obj
  ckbox_div.set_attribute('class', 'ckbox')
  ckbox_div.inner_html = "<input type='checkbox' id='checkbox#{index.to_s}'>"
  ckbox_label = Nokogiri::XML::Node.new "label", obj
  ckbox_label.set_attribute('for', "checkbox#{index.to_s}")
  ckbox_div.add_child(ckbox_label)
  col.add_child(ckbox_div)
  col
end

def build_star_col(obj)
  # This is another part of the template - I opt to insert html directly, bypassing obj construction
  # Either technique works - the below method is less fliexble because its "hard coded"
  col = Nokogiri::XML::Node.new "td", obj
  col.inner_html = '<a href="javascript:;" class="star"><i class="glyphicon glyphicon-star"></i></a>'
  col
end

def build_author_link(obj, url, img_url)
  link = Nokogiri::XML::Node.new "a", obj
  link.set_attribute('href', url)
  link.set_attribute('class', 'pull-left')
  if img_url != nil
    link.inner_html = "<img src=\"#{img_url}\" class=\"media-photo\">"
  end
  link
end

def build_article_div(obj, pub_date, title, link, author, body)
  article = Nokogiri::XML::Node.new "div", obj
  article.set_attribute('class', 'media-body')
  datespan = Nokogiri::XML::Node.new "span", obj
  datespan.set_attribute('class', 'media-meta pull-right')
  if pub_date != nil
    datespan.inner_html = pub_date
  end
  titleblock = Nokogiri::XML::Node.new "h4", obj
  titleblock.set_attribute('class', 'title')
  if title != nil
    titlelink = Nokogiri::XML::Node.new "a", obj
    titlelink.set_attribute('href', link)
    titlelink.add_child(titleblock)
    titleblock.inner_html = title
  end
  if author != nil
    authorspan = Nokogiri::XML::Node.new "span", obj
    authorspan.set_attribute('class', 'pull-right pagado')
    authorspan.inner_html = '(' + author + ')'
    titleblock.add_child(authorspan)
  end
  article.add_child(datespan)
  article.add_child(titlelink)
  if body != nil
    bodyp = Nokogiri::XML::Node.new "p", obj
    bodyp.set_attribute('class', 'summary')
    bodyp.inner_html = "#{body} (#{author})"
    article.add_child(bodyp)
  end
  article
end

def build_article_col(obj, author_link, author_thumbnail, pub_date, title, link, author, body)
  # This is the "master assembler" for each row of articles. It uses the child functions above to assemble the row
  col = Nokogiri::XML::Node.new "td", obj
  mediadiv = (Nokogiri::XML::Node.new "div", obj)
  mediadiv.set_attribute('class', 'media')
  mediadiv.add_child(build_author_link(obj, url=author_link, img_url=author_thumbnail))
  mediadiv.add_child(build_article_div(obj, pub_date=pub_date, title=title, link=link, author=author, body=body))
  col.add_child(mediadiv)
  col
end

def assemble_html(remote_obj, local_obj, remote_root)
  # This is the "master assembler" for the whole new html page. It starts with the remote html, and merges it with
  # the html generated by the build_article_col function into the "tbody" tag, removing the "placeholder" content.
  remote_articles_xpath = '//html//body//div[@class="updates"]//div[@class="newsticker-jcarousellite"]/ul/li'
  local_articles_xpath =
      '//html//body//div[@id="article-list"]//section//div//div//div//div[@class="table-container"]//table'
  remote_articles = get_article_elements(remote_obj, remote_articles_xpath)
  block = Nokogiri::XML::Node.new "tbody", local_obj
  remote_articles.each_with_index do |node, index|
    row = Nokogiri::XML::Node.new "tr", local_obj
    row.set_attribute('data-status', 'staff') # if author was present we could set this to a slug of the author
    checkbox_col = build_checkbox_col(local_obj, index=index)
    star_col = build_star_col(local_obj)
    title = node.xpath("a").text
    link = node.xpath("a/@href").text
    date = node.xpath("h2").text
    body = node.text
    article_col = build_article_col(local_obj,
                                    author_link = '#',
                                    author_thumbnail=
                                        'http://bht.virginia.edu/system/photos/1/small/BHT_Ritterband_2009.jpg',
                                    pub_date=date,
                                    title=title,
                                    link="#{remote_root}#{link}",
                                    author='Staff', #use author if present
                                    body=body
    )
    row.add_child(checkbox_col)
    row.add_child(star_col)
    row.add_child(article_col)
    block.add_child(row)
  end
  body = local_obj.xpath(local_articles_xpath).first
  body.children.remove
  body.inner_html = block
  local_obj
end

# Finally, we need to serve the html out using Webrick. We subclass the AbstractServlet class into our MiniServer class
# and pass it the setup parameters (the local and remote paths of our templates). It then calls our "master assembler"
# function which passes it the html which is then served on port 9090 (or another port we might want - which is why
# we subclassed the AbstractServlet to override the defaut config values)

class MiniServer < HTTPServlet::AbstractServlet

  def initialize server, remote_url, local_path, output='html'
    super server
    @remote_url = remote_url
    @local_path = local_path
    @output = output
  end

  def do_GET(req, resp)
    remote_html_obj = get_remote_obj(@remote_url)
    local_html_template_obj = get_local_template_obj(@local_path)
    body = assemble_html(remote_html_obj, local_html_template_obj, @remote_url)
    if @output == 'html'
      resp.body = body.to_s
    elsif @output == 'json'
      resp.body = Hash.from_xml(
          body.xpath(
              '//html//body//div[@id="article-list"]//section//div//div//div//div[@class="table-container"]//table'
          ).to_xml).to_json
    else
      resp.body = 'Output Not Defined.'
    end
    raise HTTPStatus::OK
  end

end

def start_webrick(config = {})
  config.update(:Port => 9090)
  server = HTTPServer.new(config)
  yield server if block_given?
  ['INT', 'TERM'].each { |signal|
    trap(signal) { server.shutdown }
  }
  server.start
end

start_webrick { |server|
  server.mount('/', MiniServer, 'http://bht.virginia.edu', 'template.html')
  server.mount('/test', MiniServer, 'http://bht.virginia.edu', 'template.html', 'json')
}

