# bhtarticleconverter.rb -- Sample Code

External gems required: nokogiri, activesupport (requires Ruby 2.2+)

Author: Peter D Bethke

**Problem** - the BHT articles list is not in a responsive format

**Solution**  - using the Nokogiri library ([http://www.nokogiri.org/]()), the contents of the BHT main page located at [http://bht.virginia.edu]() is turned into an object, which can then be manipulated and modified in-place. The articles list is located in a div of class newsticker-jcarousellite. So, I instruct Nokogiri to capture that block using a query language called xpath. This block is then parsed by iterating over each occurance of the `<li>`, or list item element. The various important bits, such as title, date, preview etc are captured to be merged into the target template.

The original list on the BHT Site looks like this:

![Screen Shot 2016-11-18 at 5.14.11 PM.png](https://bitbucket.org/repo/X4jpkn/images/79869804-Screen%20Shot%202016-11-18%20at%205.14.11%20PM.png)

The target template is a responsive template I found by a quick search on the internet at [http://bootsnipp.com/snippets/featured/easy-table-filter](). It is a simple article list demo with some filter and checkbox/star code built in. The articles in it are contained in a `<tbody>` container. To prep the template, I removed all the rows except one - which I left for reference. I then created a number of ruby functions to build the inner parts of the articles lists using Nokogiri, which allows a programmer to assemble html or xml using objects and render them to text (html or xml) at run-time. I then re-assembled the rows in the format expected by the "incoming" template but using the data from BHT, merging the data to create the new display list. 

*Note: There are some parts of this template that do not work or are not optimal in this solution. The first issue is that all the articles are by "Staff", which is because the articles on BHT are not tagged with an author. This means that the "filter" function does not really work because there's nothing to filter, technically. Ideally the filter buttons would contain different authors or subject tags and allow filtering from that. Similarly, the author icon is Dr. Ritterband for all articles - this would change if there were more articles. The icon itself pulls from the BHT site, though. Second, the checkboxes and stars don't really do anything - they simply suggest future features like group editing, etc. Finally, the "body" of the article is a combination of the title, date, and author from the BHT site. I did this to simply provide more text than the title itself.*

![Screen Shot 2016-11-18 at 5.14.01 PM.png](https://bitbucket.org/repo/X4jpkn/images/3782033597-Screen%20Shot%202016-11-18%20at%205.14.01%20PM.png)

The advantage of this technique is that the local template can be edited outside the `<tbody>` area without disrupting the merging. 

*Note: As long as the "path" to the <tbody> remains the same, the code will work. By path, I mean the nested path, as expressed by the xpath '//html//body//div[@id="article-list"]//section//div//div//div//div[@class="table-container" ]//table'. If this "path" is changed, the xpath will not locate the block. Its also possible to write the xpath using a "fuzzy" technique that ignores the "path" but I prefer an explicit path that is more precise because the markup does not use a lot of differentiating css id values. If this script went into production there would be a lot more error trapping to account for changes like this. In addition, a test suite would be present to simulate scenarios in which this might occur.*

**Delivery** - The re-parsed html is not written to a file - rather is is passed as a variable to the mini web server Webrick, and is viewable at [http://localhost:9090/]() once the script is run.

**Update 11/19** - As proof-of-concept, the script now also outputs a json representation of the articles table object at [http://localhost:9090/test/](), using the activesupport gem.

**Future/Thoughts** - Because the articles from BHT are broken up and made into objects, they can be re-arranged in many ways, and re-formatted in many ways based on intial script parameters. The contents can also be turned into other formats like **json** (SEE ABOVE UPDATE - which would be easliy read by a javascript node appliciation, for example), or xml. Either of these formats could then be used to create a REST api (at least for reading). The articles could also be exported in .csv or .txt format, or written locally into html files. The key is that the script functions as a pass-through proxy requiring no altering of the existing BHT site. This might be a way to prototype or even introduce new layouts without having to alter the existing BHT codebase.